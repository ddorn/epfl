#include <iostream>
using namespace std;


int demander_nombre(string prompt) {

	int n(-1);
	do {
		cout << prompt;
		cin >> n;
	} while (n < 0);

	return n;
}
