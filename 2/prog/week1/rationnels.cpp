#include <iostream>
using namespace std;

struct ZeroDivisionError{};

struct rat {
	int p;
	int q;
};


rat add(rat, rat);
rat mult(rat, rat);
rat sub(rat, rat);
rat div(rat, rat);

int gcd(int a, int b);
void simplify(rat&);
void show(rat);

rat add(rat a, rat b) {
	rat r{a.p*b.q + b.p*a.q, a.q * b.q};
	simplify(r);
	return r;
}
rat mult(rat a, rat b) {
	rat r{a.p*b.p, a.q*b.q};
	simplify(r);
	return r;
}
rat sub(rat a, rat b) {
	b.p *= -1;
	return add(a, b);
}
rat div(rat a, rat b) {
	int tmp = b.p;
	b.p = b.q;
	b.q = tmp;

	return mult(a, b);
}

int gcd(int a, int b) {
	if (a < 0) {
		a = -a;
	}
	if (b < 0) {
		b = -b;
	}
	while (b != 0) {
		int r = a % b;
		a = b;
		b = r;
	}

	return a;
}

void simplify(rat& r) {
	if (r.q == 0) {
		throw ZeroDivisionError();
	}
	if (r.q < 0) {
		r.p *= -1;
		r.q *= -1;
	}
	int g = gcd(r.p, r.q);
	r.p /= g;
	r.q /= g;
}

void show(rat a) {
	simplify(a);
	if (a.q == 1) {
		cout << a.p;
	} else {
		cout << a.p << "/" << a.q;
	}
}

int main() {
	rat a{-3, 5};
	rat b{-3, 5};
	show(add(a, b));

	return 0;
}

