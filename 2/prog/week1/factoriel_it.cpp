#include <iostream>
#include "factoriel.hpp"

using namespace std;

int factoriel(int n) {
	cout << "Iterative..." << endl;

	int result(1);
	for(int i(1); i <= n; ++i) {
		result *= i;
	}
	return result;
}
