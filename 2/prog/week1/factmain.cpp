#include <iostream>
#include "demander_nombre.hpp"
#include "factoriel.hpp"
using namespace std;

int main() {
	int n = demander_nombre("Entrer un entier positif:");
	cout << factoriel(n) << endl;

	return 0;
}

