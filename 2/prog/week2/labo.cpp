#include <iostream>
#include <string>
using namespace std;

class Souris
{
	/*****************************************************
	  Compléter le code à partir d'ici
	 *******************************************************/

	private:
		double poids;
		string couleur;
		unsigned int age;
		unsigned int esperance_vie;
		bool clonee;
	public:
		Souris(double t_poids, string t_couleur, unsigned int t_age = 0, unsigned int t_esperance_vie = 36)
			: poids(t_poids), couleur(t_couleur), age(t_age), esperance_vie(t_esperance_vie), clonee(false) {
				cout << "Une nouvelle souris !" << endl;
			}

		Souris(const Souris& s)
			: poids(s.poids), couleur(s.couleur), age(s.age), esperance_vie(s.esperance_vie), clonee(true) {
				esperance_vie = esperance_vie * 4 / 5;
				cout << "Clonage d'une souris !" <<endl;
			}

		~Souris() { cout << "Fin d'une souris..." << endl; }

		void afficher() const {
			cout << "Une souris "
				<< couleur ;
			if (clonee)
				cout << ", clonee,";
			cout << " de "
				<< age
				<< " mois et pesant "
				<< poids
				<< " grammes"
				<< endl;
		}

		void vieillir() {
			 age += 1;

			 if (clonee && 2 * age > esperance_vie) {
			 	couleur = "verte";
			 }
		}

		void evolue() {
			while (age < esperance_vie) {
				vieillir();
			}
		}

		/*******************************************
		 * Ne rien modifier après cette ligne.
		 *******************************************/

}; // fin de la classe Souris

int main()
{
	Souris s1(50.0, "blanche", 2);
	Souris s2(45.0, "grise");
	Souris s3(s2);
	// ... un tableau peut-être...
	s1.afficher();
	s2.afficher();
	s3.afficher();
	s1.evolue();
	s2.evolue();
	s3.evolue();
	s1.afficher();
	s2.afficher();
	s3.afficher();
	return 0;
}
