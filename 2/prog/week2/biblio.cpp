#include <iostream>
#include <vector>
#include <string>
using namespace std;

/*******************************************
 * Completez le programme a partir d'ici.
 *******************************************/

class Auteur {
	private:
		string name;
		bool primed;
	public:

		Auteur(const Auteur&) = delete;
		Auteur(string t_name, bool t_primed = false)
			: name(t_name), primed(t_primed)
		{}

		string getNom() const { return name; }
		bool getPrix() const { return primed; }
};

class Oeuvre {
	private:
		string title;
		const Auteur& author;
		string language;
	public:
		Oeuvre(string t_title, const Auteur& t_author, string t_language)
			: title(t_title), author(t_author), language(t_language)
		{}
		Oeuvre(const Oeuvre&) = delete;

		string getTitre() const { return title; }
		const Auteur& getAuteur() const { return author; }
		string getLangue() const { return language; }

		void affiche_no_newline() const {
			cout << title
				<< ", "
				<< author.getNom()
				<< ", en "
				<< language;
		}

		void affiche() const {
			affiche_no_newline();
			cout << endl;
		}

		~Oeuvre() {
			cout << "L'oeuvre \"";
			affiche_no_newline();
			cout << "\" n'est plus disponible." << endl;
		}
};

class Exemplaire {
	private:
		const Oeuvre& oeuvre;
	public:
		Exemplaire(const Oeuvre& t_oeuvre) : oeuvre(t_oeuvre) {
			cout << "Nouvel exemplaire de : ";
			oeuvre.affiche();
		}
		Exemplaire(const Exemplaire& e) : oeuvre(e.oeuvre) {
			cout << "Copie d'un exemplaire de : ";
			 oeuvre.affiche();
		}
		~Exemplaire() {
			 cout << "Un exemplaire de : \"";
			 oeuvre.affiche_no_newline();
			 cout << "\" a été jeté !" << endl;
		}

		const Oeuvre& getOeuvre() const { return oeuvre; }
		void affiche() const {
			 cout << "Exemplaire de ";
			 oeuvre.affiche();
		}
};

class Bibliotheque {
	private:
		vector<Exemplaire*> books;
		string name;
	public:
		Bibliotheque(string t_name) : name(t_name) {
			cout << "La bibliothèque "
				<< name
				<< " est ouverte !"
				<< endl;
		}
		string getNom() const { return name; }

		void stocker(Oeuvre& o, unsigned int count = 1) {
			for (unsigned int i = 0; i < count; i++) {
				books.push_back(new Exemplaire(o));
			}
		}

		void lister_exemplaires(string langue = "") const {
			for (auto book : books) {
				if (langue == "" || langue == book->getOeuvre().getLangue()) {
					book->affiche();
				}
			}
		}

		int compter_exemplaires(const Oeuvre& o) const {
			int count = 0;
			for (auto book : books) {
				if (&book->getOeuvre() == &o) {
					++count;
				}
			}
			return count;
		}

		void afficher_auteurs(bool prix = false) {
			for (auto book : books) {
				if (!prix or book->getOeuvre().getAuteur().getPrix()) {
					cout << book->getOeuvre().getAuteur().getNom() << endl;
				}
			}
		}

		~Bibliotheque() {
			 cout << "La bibliothèque "
				 << name
				 << " ferme ses portes,"
				 << endl
				 << "et détruit ses exemplaires : "
				 << endl;
			 for (auto book : books) {
			 	delete book;
				book = nullptr;
			 }
		}

};




/*******************************************
 * Ne rien modifier apres cette ligne.
 *******************************************/

int main()
{
	Auteur a1("Victor Hugo"),
		   a2("Alexandre Dumas"),
		   a3("Raymond Queneau", true);

	Oeuvre o1("Les Misérables"           , a1, "français" ),
		   o2("L'Homme qui rit"          , a1, "français" ),
		   o3("Le Comte de Monte-Cristo" , a2, "français" ),
		   o4("Zazie dans le métro"      , a3, "français" ),
		   o5("The Count of Monte-Cristo", a2, "anglais" );

	Bibliotheque biblio("municipale");
	biblio.stocker(o1, 2);
	biblio.stocker(o2);
	biblio.stocker(o3, 3);
	biblio.stocker(o4);
	biblio.stocker(o5);

	cout << "La bibliothèque " << biblio.getNom()
		<< " offre les exemplaires suivants :" << endl;
	biblio.lister_exemplaires();

	const string langue("anglais");
	cout << " Les exemplaires en "<< langue << " sont :" << endl;
	biblio.lister_exemplaires(langue);

	cout << " Les auteurs à succès sont :" << endl;
	biblio.afficher_auteurs(true);

	cout << " Il y a " << biblio.compter_exemplaires(o3) << " exemplaires de "
		<< o3.getTitre() << endl;

	return 0;
}
