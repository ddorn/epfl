#include <iostream>
using namespace std;

char decale(char c, char debut, int decalage);
char code(char c, int decalage);
string code(string s, int decalage);
string decode(string s, int decalage);

int main() {

	/* string s("abcdefghijKLMNOPQRSTUVWXYZAaz 12~!"); */
	/* cout << decode(s, -1) << code(code(s, 7*13), 13) << endl; */
	return 0;
}

string decode(string s, int decalage) {
	return code(s, 26 - decalage);
}


string code(string s, int decalage) {
	string cesar;
	for (auto c : s) {
		cesar += code(c, decalage);
	}
	return cesar;
}

char code(char c, int decalage) {
	 if ( 'a' <= c and c <= 'z' ) {
		return decale(c, 'a', decalage);
	 }
	 if ( 'A' <= c and c <= 'Z' ) {
		 return decale(c, 'A', decalage);
	 }
	 return c;
}

char decale(char c, char debut, int decalage) {
	while ( decalage < 0 ) {
		decalage += 26;
	}
	return debut + (((c - debut) + decalage) % 26);
}
