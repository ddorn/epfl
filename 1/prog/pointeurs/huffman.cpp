#include <iomanip>
#include <iostream>
#include <memory>
#include <vector>

using namespace std;

class Node;
struct Code;
using BinaryTree = unique_ptr<Node>;
typedef vector<BinaryTree> Forest;
typedef vector<Code> CodeTable;

BinaryTree genHuffmanCode(string);
Forest genBaseNodes(string);
BinaryTree popSmall(Forest&);
size_t getWidth(const BinaryTree&);
void printTree(const BinaryTree&);
void printCode(const BinaryTree&, string = "");
size_t depth(const BinaryTree&);
void linearize(const BinaryTree&, CodeTable&, string = "");
string encode(string, CodeTable);
string encode(char, CodeTable);


class Node {
public:
	char value = '*';
	BinaryTree left;
	BinaryTree right;
	unsigned int count = 0;

	Node () : left(), right() {}
	Node (BinaryTree& l, BinaryTree& r) : left(move(l)), right(move(r)) {
		count = 0;
		if (left.get() != nullptr) {
			count += left->count;
		}
		if (right.get() != nullptr) {
			count += right->count;
		}
	}
	Node (BinaryTree&& l, BinaryTree&& r) : Node(l, r) {}
	Node (Node&& n) : value(n.value), left(move(n.left)), right(move(n.right)), count(n.count) {}
};

struct Code {
	char letter;
	string code;
};

int main() {
	string str;
	cout << "Enter a non empty sentence: ";
	getline(cin, str);

	BinaryTree huffmancode(genHuffmanCode(str));
	CodeTable table; linearize(huffmancode, table);
	string encoded = encode(str, table);

	cout << "Generated tree:" << endl;
	printTree(huffmancode);
	cout << "Huffman code table :" << endl;
	printCode(huffmancode);
	cout << "Message in binary:" << endl;
	cout << "  " << encoded << endl;
	cout << "Length of the code / length of the message :" << endl;
	cout << "  " << encoded.size() << "/" << str.size() << " = " << encoded.size() / (double)str.size() << endl;

	return 0;
}

BinaryTree genHuffmanCode(string s) {
	Forest base = genBaseNodes(s);


	while (base.size() > 1) {
		BinaryTree tree(make_unique<Node>(popSmall(base), popSmall(base)));
		base.push_back(move(tree));
	}

	return move(base[0]);
}

Forest genBaseNodes(string s) {
	Forest tropical;
	for (char c : s) {
		bool found = false;
		for (size_t i = 0; i < tropical.size() && not found; ++i) {
			if (tropical[i]->value == c) {
				++tropical[i]->count;
				found = true;
			}
		}
		if (not found) {
			BinaryTree bush(make_unique<Node>());
			bush->count = 1;
			bush->value = c;
			tropical.push_back(move(bush));
		}
	}
	return tropical;
}

BinaryTree popSmall(Forest& forest) {
	if (forest.empty()) throw -1;

	unsigned int mini = forest[0]->count;
	size_t bestIndex = 0;

	for (size_t index = 0; index < forest.size(); ++index) {
		if (forest[index]->count < mini) {
			mini = forest[index]->count;
			bestIndex = index;
		}
	}

	BinaryTree smallest = move(forest[bestIndex]);
	forest.erase(forest.begin() + bestIndex);

	return smallest;
}

void printTree(const BinaryTree& tree) {
	vector<Node*> apolos = {tree.get()};
	vector<Node*> next;

	size_t level = depth(tree);
	BinaryTree empty(make_unique<Node>());
	empty->value = ' ';

	cout << left;
	while (not apolos.empty() and level > 0) {
		cout << setw((2 << (level - 2)) - 1);

		for (auto bintree : apolos) {
			cout << ""
				<< setw(2 << (level - 1))
				<< bintree->value;
			if (bintree->left != nullptr)
				next.push_back(bintree->left.get());
			else
				next.push_back(empty.get());
			if (bintree->right != nullptr)
				next.push_back(bintree->right.get());
			else
				next.push_back(empty.get());
		}
		cout << endl;
		apolos = next;
		next.clear();
		--level;
	}
}

size_t depth(const BinaryTree& tree) {
	if (tree == nullptr) {
		return 0;
	}
	size_t depth_left = depth(tree->left);
	size_t depth_right = depth(tree->right);

	if (depth_left > depth_right) {
		return 1 + depth_left;
	} else {
		return 1 + depth_right;
	}
}

void printCode(const BinaryTree& oak, string prefix) {
	if (oak->left == nullptr || oak->right == nullptr) {
		cout << "  " << oak->value
			<< " : " << prefix << endl;
	} else {
		printCode(oak->left, prefix + "0");
		printCode(oak->right, prefix + "1");
	}
}

string encode(string s, CodeTable table) {
	string encoded = "";
	for (char c : s) {
		encoded += encode(c, table);
	}
	return encoded;
}

string encode(char c, CodeTable table) {
	for (auto code : table) {
		if (code.letter == c) {
			return code.code;
		}
	}
	return " ";
}

void  linearize(const BinaryTree& ash, CodeTable& table, string prefix) {
	if (ash == nullptr);
	else if (ash->left == nullptr || ash->right == nullptr)
		table.push_back({ash->value, prefix});
	else {
		linearize(ash->left, table, prefix + "0");
		linearize(ash->right, table, prefix + "1");
	}
}
