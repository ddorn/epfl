#include <iostream>
#include <memory>
using namespace std;

class Node;
struct OutOfBoundError;
using ChainedList = unique_ptr<Node>;

struct OutOfBoundError {
	int index;
};

class Node {
	public:
		int value;
		ChainedList child;

		Node (int t_value) : value(t_value) {}
		Node (int t_value, ChainedList& t_child) : value(t_value), child(move(t_child)) {}

		size_t length() {
			if (child.get() == nullptr) return 1;
			else return 1 + child->length();
		}

		void insert(int v, int index) {
			if (index < 0) {
				index += length();
			}
			if (index < 0) {  // Still negative
				index -= length();
				throw OutOfBoundError({index});
			}

			if (index == 0) {
				child = make_unique<Node>(value, child);
			}
			else if (child.get() == nullptr) {
				throw OutOfBoundError{index};
			} else {
				child->insert(v, index - 1);
			}

		}

		void append(int v) {
			insert(v, length());
		}

		int at(int index) {
			if (index == 0) return value;
			if (child.get() == nullptr) throw OutOfBoundError{index};
			return child->at(index - 1);
		}
};

int readInt(string prompt) {
	int value(0);

	cout << prompt;
	while (not (cin >> value)) {
		cin.clear();
		cin.ignore(20000, '\n');
		cout << "Nope. " << prompt;
	}

	return value;
}

ChainedList inputList() {
	int size;
	do {
		size = readInt("Size of the list: ");
	} while (size < 2);

	ChainedList list = nullptr;
	for (int i = 0; i < size; ++i) {
		list->append(readInt(to_string(i) + "th value "));
	}

	return list;
}

void print(ChainedList& list) {
	Node* current = list.get();
	cout << "[ ";
	while (current != nullptr) {
		cout << current->value << " ";
		current = current->child.get();
	}
	cout << "]";
}

int main() {

	ChainedList list = inputList();
	print(list);

	cout << "Size : " << list->length() << "  "
		<< endl;
	return 0;
}

