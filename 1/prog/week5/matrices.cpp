#include <iostream>
#include <vector>

using namespace std;

vector<vector<double>> inputMatrix();
vector<vector<double>> multmat(vector<vector<double>>&, vector<vector<double>>&);
void printMatrix(vector<vector<double>>);


int main() {

	auto mat = inputMatrix();
	auto mat2 = inputMatrix();
	if ( mat2.size() == mat[0].size() ) {
		auto res = multmat(mat, mat2);
		printMatrix(res);
	} else {
		cout << "T'es nul" << endl;
	}

	return 0;
}

vector<vector<double>> inputMatrix() {
	int l1, c1;
	cout << "Nb de lignes : ";
	cin >> l1;
	cout << "Nb de colonnes : ";
	cin >> c1;

	vector<vector<double>> matrix;
	for (int l(0); l < l1; ++l) {
		vector<double> line;
		for (int c(0); c < c1; ++c) {
			cout << "M[" << l << ", " << c << "]: ";
			int number;
			cin >> number;
			line.push_back(number);
		}
		matrix.push_back(line);
	}
	return matrix;
}

void printMatrix(vector<vector<double>> matrix) {
	for ( auto l : matrix ) {
		for ( auto c : l ) {
			cout << c << " ";
		}
		cout << endl;
	}
}

vector<vector<double>> multmat(vector<vector<double>>& mat, vector<vector<double>>& mat2) {
	vector<vector<double>> res;

	size_t l1 = mat.size();

	size_t l2 = mat2.size();
	size_t c2 = mat2[0].size();


	for (size_t l(0); l < l1; ++l) {
		vector<double> line;
		for (size_t c(0); c < c2; ++c) {
			// Calcul de M[l, c]
			double sum(0);
			for (size_t k(0); k < l2; ++k) {
				sum += mat[l][k] * mat2[k][c];
			}
			line.push_back(sum);
		}
		res.push_back(line);
	}

	return res;
}
